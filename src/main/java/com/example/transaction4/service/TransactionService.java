package com.example.transaction4.service;

import com.example.transaction4.dto.TransactionDto;

import java.util.UUID;

public interface TransactionService {

    String hold(TransactionDto transactionDto);
    String confirm  (UUID id ) throws InterruptedException;


}
