package com.example.transaction4.service;


import com.example.transaction4.dto.UserDto;

public interface UserService {

//- Register (phone, pasword)
//- Login (phone, password)
//- Delete
//

    UserDto register(String phone,String password);
    UserDto login(String phone,String password);
    boolean delete();
                      }
