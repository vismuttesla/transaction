package com.example.transaction4.service.component;

import com.example.transaction4.entity.Card;
import com.example.transaction4.entity.Transaction;
import com.example.transaction4.repository.CardRepository;
import com.example.transaction4.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

import static com.example.transaction4.entity.Status.ERROR;
import static com.example.transaction4.entity.Status.SUCCESS;

@Component
@RequiredArgsConstructor
public class Confirmer {
    private final TransactionRepository transactionRepository;
    private final CardRepository cardRepository;

    @Resource(name="redisTemplate")
    private RedisTemplate<UUID,UUID> redisTemplate;

    @Resource(name="listOps")
    private ListOperations<UUID,UUID> listOps;

    public Boolean isContains(UUID uuid)
    {
        if (redisTemplate.hasKey(uuid))   return true;
        else return false;

    }

    public UUID get(UUID uuid){

        List<UUID> l=listOps.range(uuid,0,0);

        UUID id = l.get(0);
        return id;
    }




    public String check(UUID uuid) {

        Transaction transaction = transactionRepository.findById(uuid).get();

        Long cardId = transaction.getSenderCardId();
        Long amount = transaction.getSenderAmount();
        if (transaction.getStatus() != SUCCESS) {
            Card card = cardRepository.findById((cardId)).get();


            if (card.getBalance() >= amount) {
                card.setBalance(card.getBalance() - amount);

                cardRepository.save(card);

                Card receiverCard = cardRepository.findById(transaction.getReceiverCardId()).get();
                Long rAmount = transaction.getReceiverAmount();
                receiverCard.setBalance(receiverCard.getBalance() + rAmount);
                cardRepository.save(receiverCard);
                transaction.setStatus(SUCCESS);
                transactionRepository.save(transaction);
                return "SUCCESS";
            } else {
                transaction.setStatus(ERROR);
                transactionRepository.save(transaction);
                return "ERROR (you do not have enough money to transaction)";
            }
        }
        return "You send this transaction before";
    }
    public void add(UUID uuid) throws InterruptedException {
        listOps.leftPush(uuid,uuid);

    }


}

