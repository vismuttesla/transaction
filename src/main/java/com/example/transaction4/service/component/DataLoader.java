package com.example.transaction4.service.component;


import com.example.transaction4.entity.Rate;
import com.example.transaction4.repository.RateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DataLoader  implements ApplicationRunner {

        private final RateRepository rateRepository;

        public void run(ApplicationArguments args) {
       if (rateRepository.findById(1L)!=null)
            rateRepository.save(new Rate("USD","UZS",11380L));
        }


}
