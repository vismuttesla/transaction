package com.example.transaction4.service;

import com.example.transaction4.dto.CardDto;
import com.example.transaction4.dto.CardItemDto;

import java.util.List;

public interface CardService {
       CardDto add (CardItemDto cardItemDto);
       List<CardItemDto> myCard();
       CardDto cardInfo(String cardnumber);



}
