package com.example.transaction4.service.Impl;

import com.example.transaction4.dto.CardDto;
import com.example.transaction4.dto.CardItemDto;
import com.example.transaction4.entity.BalanceType;
import com.example.transaction4.entity.Card;
import com.example.transaction4.entity.Currency;
import com.example.transaction4.entity.User;
import com.example.transaction4.repository.CardRepository;
import com.example.transaction4.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {
    private final CardRepository cardRepository;


    @Override
    public CardDto add(CardItemDto cardItemDto) {
        Card card = new Card();
        card.setBalance(cardItemDto.getBalance()*100);
        card.setName(cardItemDto.getName());
        card.setCardNumber(cardItemDto.getCardNumber());
        if (cardItemDto.getCardNumber().indexOf("9860") == 0) {
            card.setCurrency(Currency.UZS);
            card.setType(BalanceType.HUMO);
        } else if (cardItemDto.getCardNumber().indexOf("4260") == 0) {
            card.setCurrency(Currency.USD);
            card.setType(BalanceType.VISA);
        }

        return CardDto.fromCard(cardRepository.save(card));
    }

    @Override
    public List<CardItemDto> myCard() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        UUID uuid = user.getId();

        return cardRepository
                .findAllByUserId(uuid)
                .stream()
                .map(CardItemDto::fromCard).collect(Collectors.toList());

    }


    @Override
    public CardDto cardInfo(String cardNumber) {
      return CardDto.fromCard(cardRepository.findByCardNumber(cardNumber));
    }


}
