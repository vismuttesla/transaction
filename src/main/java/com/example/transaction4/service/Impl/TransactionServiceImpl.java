package com.example.transaction4.service.Impl;

import com.example.transaction4.dto.TransactionDto;
import com.example.transaction4.entity.Card;
import com.example.transaction4.entity.Currency;
import com.example.transaction4.entity.Transaction;
import com.example.transaction4.entity.User;
import com.example.transaction4.repository.CardRepository;
import com.example.transaction4.repository.RateRepository;
import com.example.transaction4.repository.TransactionRepository;
import com.example.transaction4.service.TransactionService;
import com.example.transaction4.service.component.Confirmer;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import static com.example.transaction4.entity.Status.NEW;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final RateRepository rateRepository;
    private final TransactionRepository transactionRepository;
    private final CardRepository cardRepository;
    private final Confirmer confirmer;

    @Override
    @Transactional
    public String hold(TransactionDto transactionDto)
    {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        UUID uuid = user.getId();
        Card card = cardRepository.getReferenceById(transactionDto.getSenderCardId());
        UUID uuid1 = card.getUser().getId();

        if (uuid1.equals(uuid) ) {
            Transaction transaction = Transaction
                    .builder()
                    .senderCardId(transactionDto.getSenderCardId())
                    .receiverCardId(transactionDto.getReceiverCardId())
                    .senderAmount(transactionDto.getSenderAmount()).status(NEW).build();

            Currency sC = cardRepository.findById(transactionDto
                    .getSenderCardId()).get().getCurrency();
            Currency rC = cardRepository.findById(transactionDto
                    .getReceiverCardId()).get().getCurrency();

            if (sC.equals(rC)) {
                transaction.setReceiverAmount(transactionDto.getSenderAmount());

            } else {
                Long r = rateRepository.findById(1L).get().getRate();
                if (sC == Currency.UZS) {
                    Long rAmount = transactionDto.getSenderAmount() / r;
                    transaction.setSenderAmount(rAmount * r);
                    transaction.setReceiverAmount(rAmount);
                } else {
                    transaction.setReceiverAmount(transactionDto.getSenderAmount() * r);
                }
            }


            //        CardItemDto.CardItemDtoBuilder().name(card.getName()).balance(card.getBalance())
//                .cardNumber(card.getNumber()).build();


            return transactionRepository.save(transaction).getId().toString();
                               }
        else
            return "You can not pay from other person's card!";
    }

    @Override
    @Transactional
    public String confirm(UUID id) throws InterruptedException {


        if (!confirmer.isContains(id)) {
            confirmer.add(id);
            UUID uuid = confirmer.get(id);

            return confirmer.check(uuid);
        }
        else  return "you send one transaction more than once";

            }


}
