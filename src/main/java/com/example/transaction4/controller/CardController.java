package com.example.transaction4.controller;

import com.example.transaction4.dto.CardItemDto;
import com.example.transaction4.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/transaction4/card")
@RequiredArgsConstructor
public class CardController {

    private final CardService cardService;

   @GetMapping("/my_cards")
   public ResponseEntity mycard(){
       return ResponseEntity.ok(cardService.myCard());
   }

    @PostMapping("/add")
    public ResponseEntity add(@RequestBody CardItemDto cardItemDto) {
        return ResponseEntity.ok(cardService.add(cardItemDto));
    }

    @PostMapping("/cardinfo")
    public ResponseEntity cardInfo(@RequestBody String number) {

        return ResponseEntity.ok(cardService.cardInfo(number));

   }



}
