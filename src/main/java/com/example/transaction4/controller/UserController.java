package com.example.transaction4.controller;

import com.example.transaction4.Auth.AuthenticationResponse;
import com.example.transaction4.Auth.AuthenticationService;
import com.example.transaction4.dto.UserDto;
import com.example.transaction4.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/transaction4/user")
public class UserController {

    private final AuthenticationService service;

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody UserDto userDto)
    {
        return ResponseEntity.ok(service.register(userDto));
    }


    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody UserDto userDto)
    {
        return ResponseEntity.ok(service.authenticate(userDto));
    }

    @PostMapping("/delete")
    public ResponseEntity<Boolean> delete()
    {

        return ResponseEntity.ok(service.delete());


    }






}
