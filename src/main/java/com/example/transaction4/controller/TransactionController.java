package com.example.transaction4.controller;


import com.example.transaction4.dto.TransactionDto;

import com.example.transaction4.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("api/transaction4/card")
@RequiredArgsConstructor
public class TransactionController {

    private final TransactionService transactionService;
    @PostMapping("/hold")
      public ResponseEntity hold(@RequestBody TransactionDto transactionDTO)
    {
       return  ResponseEntity.ok(transactionService.hold(transactionDTO));

    }

    @PostMapping("/confirm")
    public ResponseEntity confirm (@RequestParam UUID uuid) throws InterruptedException {

        return ResponseEntity.ok(transactionService.confirm(uuid));
    }
               }
