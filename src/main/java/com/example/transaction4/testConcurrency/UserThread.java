package com.example.transaction4.testConcurrency;

import com.example.transaction4.service.TransactionService;
import lombok.RequiredArgsConstructor;

import java.util.UUID;
@RequiredArgsConstructor
public class UserThread extends Thread{



    UUID uuid;
    private final TransactionService transactionService;
    private final Massiv massiv;
   private int i;

   UserThread(int i, UUID uuid, TransactionService transactionService, Massiv massiv){
       super(i+"-Thread");
       this.uuid=uuid;
      this.transactionService= transactionService;
       this.i=i;
      this.massiv=massiv;
   }



    @Override
    public void run() {

        try {

            massiv.Response.put(transactionService.confirm(uuid));


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

                  }










