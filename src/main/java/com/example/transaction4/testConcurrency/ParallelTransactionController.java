package com.example.transaction4.testConcurrency;


import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.UUID;

@RestController
@RequestMapping("api/transaction4/card/confirm/parallel")
@RequiredArgsConstructor
public class ParallelTransactionController {

    private final Parallel parallel;
    @PostMapping
    public ResponseEntity confirm(@RequestBody ArrayList<UUID> uuid) throws InterruptedException {
               
        return ResponseEntity.ok(parallel.threads(uuid));
    }





}
