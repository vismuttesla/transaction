package com.example.transaction4.testConcurrency;

import com.example.transaction4.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;

@Component
@RequiredArgsConstructor
public class Parallel {

     private final TransactionService transactionService;
    private  final Massiv massiv;

        BlockingQueue<String> threads(ArrayList<UUID> uuid) throws InterruptedException {

       for (int i=1;i<10000;i++)
       {
             UserThread th=new UserThread(i,uuid.get((i%uuid.size())),transactionService,massiv);
              th.start();
              th.join();
       }

         return massiv.Response;

   }



                      }
