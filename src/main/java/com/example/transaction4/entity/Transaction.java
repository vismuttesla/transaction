package com.example.transaction4.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class Transaction {

    @Id
    @GeneratedValue(generator = "UUID")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    private Long senderCardId;
    private Long receiverCardId;

    private Long senderAmount;
    private Long receiverAmount;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false,length = 10)

    private Status status;

    @CreatedDate
    private Long time;

        }
