package com.example.transaction4.entity;

import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import reactor.util.annotation.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EntityListeners(AuditingEntityListener.class)
public class Card {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
      private Long id;
      private Long balance;
      private String name;

     @Column(unique = true)
     @Size(max = 16)
      private String cardNumber;

     @Enumerated(EnumType.STRING)
     @Column(nullable = false,length = 10)
      private BalanceType type;

     @Enumerated(EnumType.STRING)
     @Column(nullable = false,length = 10)
     private Currency currency;


     @ManyToOne(fetch = FetchType.LAZY)
     @CreatedBy
     private User user;



}
