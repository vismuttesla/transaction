package com.example.transaction4.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Rate {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Size(max=10)
    private String fromCurrency;

    @Size(max=10)
    private String toCurrency;

    private Long rate;

    public Rate(String fromCurrency, String toCurrency, Long rate)
    {
        this.fromCurrency=fromCurrency;
        this.toCurrency=toCurrency;
        this.rate=rate;

    }



}
