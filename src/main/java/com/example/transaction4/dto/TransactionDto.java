package com.example.transaction4.dto;

import com.example.transaction4.entity.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {
      private Long senderCardId;
      private Long receiverCardId;
      private Long senderAmount;

            }
