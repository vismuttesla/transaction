package com.example.transaction4.dto;

import com.example.transaction4.entity.BalanceType;
import com.example.transaction4.entity.Card;
import com.example.transaction4.entity.Currency;
import com.example.transaction4.entity.User;
import lombok.*;

import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CardDto
{

    private Long balance;
    private String name;

    private String cardNumber;

    private BalanceType type;

    private Currency currency;

    private UUID userId;

        public static CardDto fromCard(Card card)
                     {

            CardDto cardDto=new CardDto() ;
            cardDto.setBalance(card.getBalance());
            cardDto.setName(card.getName());
            cardDto.setCardNumber(card.getCardNumber());
            cardDto.setType(card.getType());
            cardDto.setCurrency(card.getCurrency());
            cardDto.setUserId(card.getUser().getId());

           return  cardDto;

                      }




}
