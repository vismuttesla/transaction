package com.example.transaction4.dto;

import com.example.transaction4.entity.Card;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CardItemDto {
    private Long balance;
    private String name;
    private String cardNumber;

    public static CardItemDto fromCard(Card card) {

        CardItemDto cardItem = new
                CardItemDtoBuilder().name(card.getName()).balance(card.getBalance())
                .cardNumber(card.getCardNumber()).build();
        return cardItem;


    }


}