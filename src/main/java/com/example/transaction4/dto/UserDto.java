package com.example.transaction4.dto;

import com.example.transaction4.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
      private String  phone;
      private String password;

    public static UserDto fromUser(User user)
    {
        UserDto userDto=new UserDto();
        userDto.setPassword(user.getPassword());
        userDto.setPhone(user.getPhone());
        return userDto;
    }


}
