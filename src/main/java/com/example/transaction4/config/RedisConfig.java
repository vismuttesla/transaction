package com.example.transaction4.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.UUID;

@Configuration
public class RedisConfig {
    @Bean
    public JedisConnectionFactory redisConnectionFactory() {

        RedisStandaloneConfiguration config =
                new RedisStandaloneConfiguration("localhost", 6379);

        return new JedisConnectionFactory(config);


    }

    @Bean(name = "redisTemplate")

    public RedisTemplate<UUID, UUID> redisTemplate() {
        RedisTemplate<UUID, UUID> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory());

        return redisTemplate;
    }

    @Bean(name = "listOps")
    public ListOperations<UUID, UUID> listOperations() {

        return redisTemplate().opsForList();
    }
}