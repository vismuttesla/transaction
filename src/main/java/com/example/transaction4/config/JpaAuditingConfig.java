package com.example.transaction4.config;

import com.example.transaction4.entity.User;
import com.example.transaction4.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration

public class JpaAuditingConfig {
    @Bean
    public AuditorAware<User> userAuditorAware() {
        return () -> {
            SecurityContext context = SecurityContextHolder.getContext();
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();


            if (context != null && context.getAuthentication() != null) {


                return Optional.ofNullable(((User) authentication.getPrincipal()).getUser());
            }
            return Optional.empty();
        };
    }
}