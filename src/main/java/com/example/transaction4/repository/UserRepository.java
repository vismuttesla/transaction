package com.example.transaction4.repository;

import com.example.transaction4.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface UserRepository extends JpaRepository<User,Long>
                        {

        Optional<User> findByPhone(String phone);
               User findUserByPhone(String phone);

                        }
