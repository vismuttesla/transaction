package com.example.transaction4.repository;

import com.example.transaction4.entity.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public interface CardRepository extends JpaRepository<Card,Long> {

      Optional<Card> findById(Long id);

      List<Card> findAllByUserId(UUID uuid);

      Card findByCardNumber(String number);





}
