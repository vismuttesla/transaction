package com.example.transaction4.Auth;

import com.example.transaction4.dto.UserDto;
import com.example.transaction4.entity.User;
import com.example.transaction4.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.example.transaction4.entity.UserStatus.ACTIVE;
import static com.example.transaction4.entity.UserStatus.DELETED;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;

    public AuthenticationResponse register(UserDto userDto) {

        var user = User.builder()
                .phone(userDto.getPhone())
                .password(passwordEncoder.encode(userDto.getPassword()))
                .status(ACTIVE)
                .build();
        repository.save(user);


        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public AuthenticationResponse authenticate(UserDto userDto) {
        User user1 = repository.findUserByPhone(userDto.getPhone());
        if (user1.getStatus() != DELETED) {
            authenticationManager.authenticate(

                    new UsernamePasswordAuthenticationToken(
                            userDto.getPhone(),
                            userDto.getPassword()

                    )
            );
        }
        var user = repository.findByPhone(userDto.getPhone())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        return AuthenticationResponse.builder()
                .token(jwtToken)
                .build();
    }

    public Boolean delete() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        String phone = auth.getName();
        User user = repository.findUserByPhone(phone);
        user.setStatus(DELETED);

        if (repository.save(user).getStatus() == DELETED)


            return true;
                      else return false;


    }


}



